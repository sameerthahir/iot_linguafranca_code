# Contains the reactor definitions for the Things and Edge devices that are mapped to various resources in the health-center
target Python;

# Base Reactors

reactor Data_Source(sense_interval(10secs)){
    # Considered as a unintelligent node producing data stream(s)
    
    input physical_data
    physical action trigger
    
    output data
    
    pow_src=new Power_Source()
        
    reaction(physical_data)->data{=
        data.set(physical_data.value)
    =}
}


reactor Power_Source(capacity(1000),type("LI")){
    
    physical action p_charging
    physical action p_discharging
    physical action p_empty
    output charge
    state charge_level
    state capacity
    
    initial mode discharging{
        reaction(p_charging)->charging{=
            
            charging.set()
        =}
        
        reaction(p_empty)->empty{=
            empty.set()
        =}
        
    } 
    
    mode charging{
        
        reaction(p_discharging)->discharging{=
            discharging.set()
        =}
        
    }
    
    mode empty{
        
        reaction(p_charging)->charging,charge{=
        charging.set()
        charge.set(self.charge_level)
    =}
    
    
        
    }
    
    reaction(startup){=
        
    =}
    
    
}


reactor Things_Devices{
    input commands
    mutable input charge_action
    output data
    output charge
    
    state commn_interfaces # List of communication interfaces
    state commn_protocols # List of communication protocols(hw/sw)
    state device_id # NDN to be used ?
    state sensors_connected #list of sensors(encoded inf.)
    state actuators_connected #list of actuators(encoded inf.)
    state power_source_type
    state power_remaining
    
    state iot_layer # things, edge, cloud
    state agg_id # whom to send data
    state edge_details # a longshot at which edge agent deals with my data
    
    pow_src=new Power_Source()
    
    
    
    # {
    # RAM : "8GB"
    # CPU : "4cores"
    # Disk : "32GB"
    # NetworkInterface: { {"Lora":1, BLE:0, Wifi:1, Eth:0}}
    # Power Source: "AC"
    # Internet connectivity : "Wifi"
    # }
    state capabilities # json file with all details 
    
    state human_interface # the interaction with the device
    state configuration # resource description
        
    reaction(startup){=
        print("initialized main things reactor")
    =}    
        
}

# Derived Things Reactors

reactor SAD_Device extends Things_Devices{
    
    # TODO: Duplicate names in parent reactor
   
    input physical_observations
    input actions
    input configure_cmd
    
    output ground_data
    output sad_actions
    
    timer timer_send_data_battery(30sec)
    timer timer_send_data_charging(1min)
    timer timer_send_data_battery_critical(5min)
    
    
    state sens_or_act #boolean 1- sensor 0 - actuator
    physical action p_actuation
    physical action p_battery
    physical action p_charge
    physical action p_battery_critical
    physical action p_actuation_impact
    
    # read the sensor data, wakeup from sleep
    /*reaction(trigger){=
        
    =}*/
    
    reaction(startup){=
        print("initialized SAD device")
    =}
     
    
     
    initial mode on_charge{
        
        # When on charge, the SAD devices can fulfil all the functionalities.
        # 1. Send data at normal intervals
        # 2. Receive actions and acknowledge their effect.
        # 3. Receive and change configuration parameters 
        
        reaction(p_battery)->on_battery_normal{=
            on_battery_normal.set()
        =}
        
        reaction(timer_send_data_charging){=
            
        =}
        
        reaction(actions)->sad_actions{=
        p_actuation.schedule(0)
        
        =}
    
        reaction(p_actuation){=
        # Thread to execute action
        =}
        
        reaction(p_actuation_impact){=
            
        =}
        
        # SAD<--->Aggregator interface
        # Configure the SAD
        reaction(configure_cmd){=
        
        =}
        
    }
    
    mode on_battery_normal{
        # When on battery, the SAD devices have limited functionalities.
        # 1. Send data at infrequent intervals
        # 2. Receive actions without acknowledge their effect.
        # 3. Ignore configuration parameters
        # 4. Sleep after sending/receiving packets.
        reaction(p_charge)->on_charge{=
            on_charge.set()
        =}
        
        reaction(p_battery_critical)->on_battery_critical{=
            on_battery_critical.set()
        =}
        
       
        reaction(timer_send_data_battery){=
            
        =}
        
        reaction(actions)->sad_actions{=
        actuation.schedule(0)
        
        =}
    
        reaction(p_actuation){=
        # Thread to execute action
        =}
        
    }
    
    mode on_battery_critical{
        # When in the critical stage, the SAD devices will act only as defined. 
        # 1. Send data at fixed times as notified earlier
        # 2. Ignore actions
        # 3. Ignore configuration parameters
        
        reaction(p_charge)->on_charge{=
            on_charge.set()
        =}
        reaction(timer_send_data_battery_critical){=
            
        =}
        
    }
    
    # Send data to edge agent
    reaction (physical_observations)->ground_data{=
        ground_data.set(physical_observations.value)
    =}
}



# Currently classified as a Things Device (interface for Edge Things)
# Aggregator will always be associated with One Edge Agent.
# Edge Agents maybe associated with more than one Aggregators
# Aggregators may share SAD Devices/Data Sources
# parameter - manages 'N' number of SAD devices
# IMP=-=-=-> Although Data Sources are not directly exposed to Edge layer, the information concerning all data sources are available to the Edge through the aggregators(edge agent (1)-->(n) aggregators) 
# Aggregators also execute basic tasks provided by edge agents 
reactor Aggregator(SAD_Devices_list(1),DataSources(1)) extends Things_Devices{
    
    # Functionalities
    # 1. Associate with edge agent
    # 2. Determine edge agent failure and connect with next agent in list
    # 3. 

    preamble{=
        edge_agents=[]
        sad_devices=[]
        data_sources={}
    =}
    
    input[2] sens_data           # Sensor data from SAD
    #input associate_agent       # Associate with an edge agent
    output data_for_edge        # Forward data received from sensors to Edge Agent
    input actions               # Send actions received from Cloud
    output data_for_things       # Configuration Data/ Actions for SAD 
    input data_source_request   # Add/Remove/Change Data Source
    output edge_agent_requests  # Requests for the edge agents.
    
                # Id of the aggregator
    state SAD_list          # list of SAD devices
    state edge_agent_list   # Associated edge agents
    state SAD_Conf_values   # Send configuration values
    state current_agent_id  # Current Edge Agent ID
    
    # TODO : Physical and logical actions
    logical action l_forward_to_edge
    physical action p_conf_data_source
    timer timer_edge_data_send(10secs)
    
    method cache_SAD_data(data){=
        
    =}
    
    method send_conf_values_SAD(){=
        
    =}
    
    method determine_edge_failure(){=
        # Figure out using implementation.
    =}
    
    method connect_new_edge(){=
        
    =}
    
    method add_remove_sad_device(new_sad){=
        
    =}
    
    method add_remove_data_source(new_data_source){=
        
    =}
    
    method suggest_alternate_data_source(){=
        
    =}
    
    # Aggregator<--->SAD Device Interface
    # Each Aggregator reactor is instantiated for each sad/data source
    # TODO : Model-Design-Implementation Decision
    # This will result in several microservices, which will all run on the same node.
    
    # Aggregator abstract/hidden modes
    # 1. At least one SAD/Data source is alive
    # 2. No SAD/Data Sources are alive
    # 3. SAD/Data Sources are in critical battery
    
    # Aggregator operation modes
    # 1. Registering with an edge agent
    # 2. Registering data sources and SAD devices
    # 3. Send data to Edge Agent
    # 4. Cache Data from Data Source
    
    /*
     
     initial mode sad_normal_operation{
      
     }
      
      
     mode nil_sad_data_source{
        
        
        
    }
    
    mode sad_data_source_critical_battery{
        
    }*/    
    
    initial mode register_node{
        # Register Aggregator with edge agent
        
        reaction(startup)->edge_agent_requests, receive_things_data{=
            edge_agent_requests.set("register")
            receive_things_data.set()
        =}      
        
        /*reaction(p_edge_command){=
            
        =}*/
        
    }
    
    mode receive_things_data{
        
        reaction (sens_data)->send_edge_data{=
            print(f"sensing data%f",sens_data)
            self.cache_SAD_data(sens_data)
            l_forward_to_edge.schedule(100,sens_data)
            send_edge_data.set()
        =}
    }
    
    mode send_edge_data{
        # Aggregator<--->Edge Device Interface
    
        # Send data to Edge Agent
    
        reaction (l_forward_to_edge)->data_for_edge,receive_things_data{=
            data_for_edge.set(l_forward_to_edge.value)
            receive_things_data.set()
        =}        
    }
    
    mode register_sad_ds{
        
        reaction(data_source_request)-> receive_things_data{=
            self.data_source.append(data_source_request)
            receive_things_data.set()
        =}
        
    }
    
    mode forward_edge_commands{
        
        reaction(actions)-> receive_things_data{=
            # Find out the SAD to receive the actutation command.
            receive_things_data.set()
        =}
    }
    

    
    # receive and store data
    /*
   
    * 
    */
    /*
    # Send configuration parameters on receiving trigger from edge
    reaction(p_conf_data_source)->data_for_things{=
        self.send_conf_values_SAD()
    =}
     */
    
    
    
   
    /*
    # Associate/Disassociate with an edge agent
    reaction (associate_agent){=
        if associate_agent==1:
            self.edge_agent_list.append(associate_agent)
        else:
            self.edge_agent_list.remove(associate_agent)
    =}
    
     */
    
    /*
    # The Edge Agent is aware of the data sources.
    # EA can request new data sources to the aggregators
    # Add/Remove/Substitute a data source
    reaction(data_source_request){=
        print("Add/Remove/Substitute a Data Source")
        
    =}
     */
       
     
    reaction(startup){=
        print("initialized aggregator")
    =}    
}


 

reactor Edge_Device{
    #input data
    #output commands
    state iot_layer # things, edge, cloud
    state human_interface # the interaction with the device
    state configuration # resource description
    state commn_interfaces # List of communication interfaces
    state commn_protocols # List of communication protocols(hw/sw)
    state power_source_type
    state device_id # NDN to be used ?
    state edge_role # cache, store, compute, agent
    # {
    # RAM : "8GB"
    # CPU : "4cores"
    # Disk : "32GB"
    # NetworkInterface: { {"Lora":1, BLE:0, Wifi:1, Eth:0}}
    # Power Source: "AC"
    # Internet connectivity : "Wifi"
    # }
    state capabilities # json file with all details 
    
    
    
    reaction(startup){=
        print("initialized edge main reactor")
        
    =}   
        
}


reactor Edge_Cloud_Mode{
    
    initial mode cloud_mode{       
        
        
    }
    
    mode edge_mode{        
        
        
    }   
}


reactor Edge_Things_Mode{
    
    input aggregator_data
    
    
    initial mode things_mode{
        
        # receive data from aggregator
             
        
    }   
    
    mode edge_mode{
        
    }
}

reactor Edge_Edge_Mode{
    
    initial mode edge_cache_mode{
        
    }    
    
    mode edge_store_mode{
        
    }
    
    mode edge_compute_mode{
        
    }
}



# Derived Edge Reactors
# Edge Agent performs all the intelligent tasks in the Edge layer.
# The TD learning also starts at the edge agent. Edge agent microservices can run on any device 
reactor Edge_Agent extends Edge_Device{
    
    # Modes for edge agent reactor
    # sending/receiving data from aggregator
    # sending data to cloud
    # sending data to data cache and data store
    
    
    /*initial mode Edge_Cloud{
        
        ECMode=new Edge_Cloud_Mode()
        
    }
    
    mode Edge_Edge{
        EEMode=new Edge_Edge_Mode()
        
    }
    
    mode Edge_Things{
        ETMode=new Edge_Things_Mode()
        
    }*/
    
    
    input data_from_aggregator# data received from things layer
    #input data_source_request   # request for a new datasource
    #input register_edge     # register edge device
    input[3] edge_node_commands # Commands from other edge nodes
    input cloud_commands # actuation commands from cloud
    
    output cloud_data       # data to be send to cloud
    output agent_commands   # commands for other edge agents
    
    output agg_cmd          # Command for aggregator
    
    output[2] egde_commands    # commands for other edge devices
    
    state edge_node_list # List of device ids to be controlled
    state agent_properties # refer PADE
    state agent_network #networkx for representing the agent network(connection to other agents and edge devices(comp, cache, store). 
    
    # The logical action may be executed at certain times /time intervals or when the agent forsees a failure.
    logical action l_send_to_cloud
    logical action l_store_edge_store
    logical action l_cache_edge_cache
    logical action l_send_aggregator
    
    physical action p_edge_agent_action
    
    reaction(startup){=
        print("initialized edge agent")
        self.edge_role="agent"
    =}
    
    reaction(data_from_aggregator)->l_send_to_cloud,l_store_edge_store,l_cache_edge_cache{=
        l_send_to_cloud.schedule(0)
        l_store_edge_store.schedule(0)
        l_cache_edge_cache.schedule(0)
    =}
    
    # TB Edge already accomplishes the edge-cloud interface
    # Following reaction makes sense when we want to override that
    # Send Data to cloud capability only exisits with the edge agent
    # The edge agent(replicas) will exist as microservices in any of the edge devices
    # The parent Edge reactor can also possess this. Change the model
    reaction(l_send_to_cloud)->cloud_data{=
        cloud_data.set(l_send_to_cloud.value)
    =}
    
    reaction(l_store_edge_store)->egde_commands{=
        edge_commands.set(l_store_edge_store.value)
    =}
    
    reaction(l_cache_edge_cache)->egde_commands{=
        edge_commands.set(l_cache_edge_cache.value)
    =}
    
    # edge<->edge,cloud interface
    # Suggest Data Source
    # Use experience of local td learning to find new data sources or suggest new ones
    /*reaction(data_source_request){=
        print("Finding a new data source from experience")
    =}
    
    reaction(register_edge){=
        # Use the value of register_edge to add the edge device to the list
        # contains ID of the node
        # capabilities
        # physical space 
        self.edge_node_list.append(register_edge.value)
    =}*/
    
    reaction(cloud_commands)->l_send_aggregator{=
        l_send_aggregator.schedule(0)
    =}
    
    reaction(l_send_aggregator)->agg_cmd{=
        agg_cmd.set(l_send_aggregator.value)
    =}
    
    reaction(edge_node_commands){=
        print("Execute command")
    =}
    
    reaction(p_edge_agent_action)->agent_commands{=
        print("Take action and send commands to other agents")
    =}
  
    
}

# Federated learning reactor for nodes involved in FL
reactor FL_Compute{
    # TODO populate with FL parameters
    state fl_role("client") # client/parameter server
    state fl_algo("fl_avg") # FL algorithm used
    state client_score # retrieved from parameter score
    
    #TODO populate with FL methods
    reaction(startup){=
        
    =}    
    
}

reactor Edge_Compute extends Edge_Device{
    
    #input problem # list of data sources and algo to execute
    input agent_cmd
    output results #parameters in case of FL
    output commands
    
    state agent_id #Agent associated
    state algorithms # list of algorithms executed
    state computational_capability # what can be computed
    state computational_performance # store the results of computation (accuracy, time, think of others)
    state FL_id # if this node is part of a federated learning network, it must be tracked using FL specific parameters
    
    logical action l_new_data_source_required 
    
    flc=new FL_Compute() # if this is a FL node(client/server)
    
    
    
    method calc_comp_score(){=
        # Calculate the score based on the data used , computation performed, the results obtained and the time used for computation (data complexity) (mainly for federated learning
    =}
    
    # TODO : reactions for edge compute 
    
    reaction(startup){=
        print("initialized edge compute")
        self.edge_role="compute"
    =}
    
    # EC<--->EA interface
    # Compute nodes may request alternate data sources.
    reaction(l_new_data_source_required){=
        # Based on the accuracy of results the compute node may request alternate data sources for certain nodes.
    =}
}

# Edge Cache will store the 
# 1 .sensor data sources(time series)
# 2. physical actions, inputs and network events
# 3. TD learning , FL process data
reactor Edge_Cache extends Edge_Device{
    
    #input data
    input agent_cmd
    state agent_id #Agent associated
    
    method cache_store_action(command){=
         
    =}
    
    # EE<--->EA interface
    
    reaction(agent_cmd){=
    # Store data
    # retrieve data    
    =}
    
    
    
    
    reaction(startup){=
        print("initialized edge cache")
        self.edge_role="cache"
    =}    
}

reactor Edge_Store extends Edge_Device{
    
    #input data
    input agent_cmd
    output edge_requests
    state agent_id #Agent associated
    state store_location # Location of storage of time-series/configuration/results/CONSEQUENCES<--- data
    
    
    method db_action(command){=
       
    =}
    
    # ES<--->EA interface
    
    reaction(agent_cmd){=
        # Store data
        # retrieve data
        # update data
        # delete data    
    =}
    
    
    
    reaction(startup){=
        print("initialized edge store")
        self.edge_role="store"
    =}    
}

reactor gateway{
    input edge_data
    input commands
    input actions
    output edge_commands
    output cloud_data
    output edge_cache_data
    
    reaction(edge_data){=
        # Forward data to cloud 
    =}
    
    reaction(commands){=
        # Perform actions 
        # 1. cache
        # 2. initiate migration
        # 3. Store statistics
    =}
    
    
}
 
