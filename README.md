## Name
CPS based Reactor Model for Edge-IoT Architecture

## Description
This project is part of the Research done at the Cyber Physical Systems Lab @ Department of Computer Science, Cochin University of Science and Technology. The project attempts to develop a Architectural Model for Edge-based IoT Applications. The Model is used to implement a working application for HealthCenters. A Model for a Smart Parking Management System is also illustrated. 


## Installation

The project comprises of 3 directories
1. Reactor_IoT_Architecture - Contains the generic model for an Edge-based IoT Application
2. HealthCare_IoT - An Edge-IoT based HealthCenter application built on top of the Generic Architecture 
3. Smart_Parking_Management_System - A Smart City based application Edge-IoT built on top of the Generic Architecture
4. Reaction_traces - Reaction traces for experiments conducted with the Health Center Reactors

In order to run this experiment, please follow the instructions at lf-lang.org to setup Lingua-Franca. Once the IDE and/or compiler has been setup. You can compile the the Main Reactor using the following command. Kindly note that importing the entire project directory in the Epoch IDE (for lingua franca) will be useful to create the necessary directories. Compilation will also produce the necessary binaries in the right directory. 

```
$ /path/to/lfc Main.lf 
```

## Usage
You can find the generated python scripts in the parent directory(src-gen). For the HealthCenter use-case run the simulator python script. We have used a csv file containing data of anonymous patient based on their visit to the health-center and the facilities they have used. The data is not available. In order to create docker images for each reactor , specify ```docker:true``` ins the preamble of the reactor source code and generate the docker files which will be available in fed-gen/ directory. The docker image can be created using docker-compose command. All docker images are executed using docker-compose up. Kindly note that manual modification of docker files maybe necessary in order to include the docker images to contain the platform specific library(for example in Casualty Reactor).



## Support
Contact me @ sameerm at cusat dot ac dot in.

## Roadmap
Need to add mutation code 


## Contributing
You are welcome to collaborate with this project at the CPS lab.

## Authors and acknowledgment
Thanks to Prof. Edward Lee , Prof. Marten Lohstroh and the Lingua Franca team for the great tool

## License
GNU GPL v3.0

## Project status
As we are progressing our work on other projects, we are improvising on the Reactor Models and attempting to execute on different testbeds.
